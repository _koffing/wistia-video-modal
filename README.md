# Wistia video modal #

This plugin allows a developer to easily add a video modal to any page by adding the data-video attribute to any element they would like to be the click target for the video modal.

### How do I get set up? ###

* Include the JavaScript and css/sass into your application.
* add a div with the class .site-constraint inside the body that wraps all of the sites content and give it the position of relative.
* add the data-video attribute with the string from the iframe as the data to any element you would like to be the click target for for the video modal. 

### Examples ###

There is a full example of usage in video-modal.html which shows all of the data attributes available for the modal. Only the data-video attribute is required.

### Dependencies ###

* jQuery