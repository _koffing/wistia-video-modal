/*  video modal ***********************************************************************
 **************************************************************************************/
// must have a .site-constraint or body set to relative

(function($){
    videoModal();

    function videoModal() {
        var $siteConstraint = $('.site-constraint');
        var $body = $('body');
        var video_Attr;
        var title;
        var description;
        var video_thumb;
        var video_Attr_2;
        var title_2;
        var description_2;
        var video_thumb_2;
        var videoModal;
        var current_video = 1;
        var next_video;
        var next_video_title;
        var next_video_description;
        var next_video_img;
        var new_video;
        var new_text;
        var new_img;
        var video_disclaimer;
        var video_2;
        var disclaimer;
        $('[data-video]').on('click', function(){
            //getting video data
            video_Attr = $(this).attr('data-video');
            title = $(this).attr('data-video_title');
            description = $(this).attr('data-video_description');
            video_thumb = $(this).attr('data-video_thumbnail');
            video_Attr_2 = $(this).attr('data-video_2');
            title_2 = $(this).attr('data-video_title_2');
            description_2 = $(this).attr('data-video_description_2');
            video_thumb_2 = $(this).attr('data-video_thumbnail_2');
            video_disclaimer = $(this).attr('data-video_disclaimer');
            //check if there is a second video
            if(video_Attr_2 != '' && video_Attr_2 != undefined){
                video_2 = '<div class="video-list">' +
                    '<div  class="video-list__item">' +
                    '<div class="img-container">' +
                    '<img src="' + video_thumb_2 + '">' +
                    '</div>' +
                    '<div class="text-container">' +
                    '<h3>'+ title_2 +'</h3>' +
                    '<p>' + description_2 + '</p>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
            } else {
                video_2 = '';
            }
            //check if there is a disclaimer
            if(video_disclaimer != '' && video_disclaimer != undefined){
                disclaimer = '<em>' + video_disclaimer + '</em>';
            } else {
                disclaimer = '';
            }

            //modal template
            videoModal = $('<div class="video-modal">' +
                '<div class="video-wrapper">' +
                '<span class="video-close">close</span>' +
                '<div class="video-container">' +
                video_Attr +
                '</div>' +
                disclaimer +
                video_2 +
                '</div>' +
                '</div>');
            $siteConstraint.append(videoModal);
            setTimeout(function(){
                $body.addClass('video-modal-active');
            }, 200);
        });

        $(document).on('click', '.img-container', function(){
            video_switch();
        });

        // function used to switch witch video is playing
        function video_switch (){
            if(current_video == 1){
                next_video = video_Attr_2;
                next_video_title = title;
                next_video_img = video_thumb;
                next_video_description = description;
                current_video = 2;
            } else {
                next_video = video_Attr;
                next_video_title = title_2;
                next_video_img = video_thumb_2;
                next_video_description = description_2;
                current_video = 1;
            }
            //setting the new video data
            new_video =  next_video;
            new_text = '<h3>' + next_video_title +'</h3><p>' + next_video_description +'</p>'
            new_img = '<img src="' + next_video_img + '">';
            //switching out the data
            $('.video-container').html(new_video);
            $('.img-container').html(new_img);
            $('.text-container').html(new_text);
        }

        $(document).on('click', '.video-modal', function(e){
            var $this = $(this);
            if(event.target.className == 'video-modal'){
                $body.removeClass('video-modal-active');
                setTimeout(function(){
                    $this.remove();
                }, 500);
            }
        });
        $(document).on('click', '.video-close', function(){
            $body.removeClass('video-modal-active');
            setTimeout(function(){
                $('.video-modal').remove();
            }, 500);

        });
    }
})(jQuery);
